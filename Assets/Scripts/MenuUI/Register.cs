using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Michsky.UI.Zone;
public class Register : MonoBehaviour
{
    public MainPanelManager mainPanel;
    public SwitchManager switchM;
    public TMP_Text bcState;
    public TMP_InputField nickname;
    public TMP_InputField password;
    public TMP_InputField email;
    void Start()
    {
        
    }
    public void RegisterButton()
    {
        if (switchM.isOn)
        {
            StartCoroutine("IRegister");
        }
        else
        {
            bcState.text = "Please accept the user terms";
        }
        
    }
    IEnumerator IRegister()
    {
        NetworkSettings.instance.Register(nickname.text, password.text, email.text);
        yield return new WaitUntil(() => NetworkSettings.instance.bcRegister);
        NetworkSettings.instance.Login(email.text, password.text);
        mainPanel.PanelAnim(0);
    }
}
