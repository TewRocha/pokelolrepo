using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Michsky.UI.ModernUIPack;
public class DropdownFilter : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField inputField;
    [SerializeField]
    private TMP_Text selectedDropdownItem;
    [SerializeField]
    private CustomDropdown dropdown;

    private void OnEnable()
    {
        inputField.text="";
        dropdown.dropdownItemsFilter = dropdown.dropdownItems;
    }
    public void FilterDropdown()
    {
        dropdown.dropdownItems = dropdown.dropdownItemsFilter.FindAll(option => option.itemName.IndexOf(inputField.text) >= 0);
        dropdown.SetupDropdown();
    }
    public void SetInputFildByDropdown()
    {
        inputField.text = selectedDropdownItem.text;
    }
}
