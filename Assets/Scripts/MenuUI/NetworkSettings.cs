using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using PlayFab;
using PlayFab.ClientModels;
public class NetworkSettings : MonoBehaviourPunCallbacks
{
    public static NetworkSettings instance;
    public string photonState, bcState;
    public bool photonConnected,bcConnected,bcRegister;
    public string m_username = "";
    public string m_password = "";
    public string m_email = "";
    public string username;
    public int allCreaturesLoaded;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        PhotonNetwork.ConnectUsingSettings();
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            PlayFabSettings.staticSettings.TitleId = "F3B49";
        }
        //var request = new LoginWithCustomIDRequest { CustomId = "GettingStartedGuide", CreateAccount = true };
        //PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
        photonState = "Connecting to server";

    }
    public override void OnConnectedToMaster()
    {
        photonConnected = true;
        photonState = "Connected";
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        photonState = cause.ToString();
    }
    public void Login(string email, string password)
    {
        m_email = email;
        m_password = password;
        if (m_email != "" && m_password != "")
        {
            var request = new LoginWithEmailAddressRequest { Email = m_email, Password = m_password};
            PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
        }
        else
        {
            bcState = "Nickname or password empty";
        }
    }
    public void Register(string username, string password, string email)
    {
        m_username = username;
        m_password = password;
        m_email = email;
        if (m_username != "" && m_password != "" && m_email != "")
        {
            var registerResquest = new RegisterPlayFabUserRequest { Email = m_email, Password = m_password, Username = m_username };
            PlayFabClientAPI.RegisterPlayFabUser(registerResquest, OnRegisterSucess, OnRegisterFailure);
        }
        else
        {
            bcState = "Nickname, password or email is empty";
        }
    }
    private void OnLoginSuccess(LoginResult resultado)
    {
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest() {
            PlayFabId = resultado.PlayFabId,
            ProfileConstraints = new PlayerProfileViewConstraints() {
                ShowDisplayName = true }
        },
        result => username = result.PlayerProfile.DisplayName,
        error => Debug.Log("Deu erro"));

        AcessCreatures.instance.LoadTeams(resultado.PlayFabId);
        AcessCreatures.instance.LoadMainTeam(resultado.PlayFabId);
        bcConnected = true;
    }

    private void OnLoginFailure(PlayFabError error)
    {
        bcState = "Login failed";
    }
    private void OnRegisterSucess(RegisterPlayFabUserResult result)
    {
        bcState = "Sucessfull registrations";
        bcRegister = true;
    }
    private void OnRegisterFailure(PlayFabError error)
    {
        bcState = "Register failed";
    }
}
