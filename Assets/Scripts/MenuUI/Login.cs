using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Michsky.UI.Zone;
public class Login : MonoBehaviour
{
    public Animator splashScreenAnim;
    public AudioSource splashScreenAudio;
    public GameObject mainPanels;
    public Animator mainPanelsAnim;
    public Animator home;
    public SwitchManager rememberme;
    public TMP_Text profileName;

    public TMP_Text photonState,bcState;
    public TMP_InputField email;
    public TMP_InputField password;
    public bool photonConnected, bcConnected,logado;

    void Start()
    {
        string remember = PlayerPrefs.GetString("remember");
        if (remember == "true")
        {
            email.text = PlayerPrefs.GetString("email");
            password.text = PlayerPrefs.GetString("password");
            //LoginButton();
        }
        
    }
    private void Update()
    {
        photonState.text = NetworkSettings.instance.photonState;
        bcState.text = NetworkSettings.instance.bcState;
    }
    public void LoginButton()
    {
        StartCoroutine("ILogin");
    }
    IEnumerator ILogin()
    {
        NetworkSettings.instance.Login(email.text, password.text);
        yield return new WaitUntil(()=> NetworkSettings.instance.bcConnected);
        if (rememberme.isOn)
        {
            PlayerPrefs.SetString("email", email.text);
            PlayerPrefs.SetString("password", password.text);
            PlayerPrefs.SetString("remember", "true");
        }
        else
        {
            PlayerPrefs.SetString("remember", "false");
        }
        AcessCreatures.instance.SetCreatures();
        AcessCreatures.instance.SetMoves();
        AcessCreatures.instance.SetItens();
        splashScreenAnim.Play("Login Screen Out");
        splashScreenAudio.Play();
        yield return new WaitForSeconds(0.1f);
        mainPanels.SetActive(true);
        mainPanelsAnim.Play("Start");
        home.Play("Wait");
        profileName.text = NetworkSettings.instance.username;
    }
    
}
