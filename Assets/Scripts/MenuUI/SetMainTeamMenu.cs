using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using TMPro;

public class SetMainTeamMenu : MonoBehaviour
{
    [SerializeField]
    private Animator loadOut;
    [SerializeField]
    private List<Image> icons;
    [SerializeField]
    private List<Image> prevIcons;
    [SerializeField]
    private HorizontalSelector horizontalSelector;
    [SerializeField]
    private TMP_Text menssage;
    //[SerializeField]
    //private TMP_Text prevTitle;
    private int teamNumber;
    private void Start()
    {
        SetPreVisualizeIcons();
    }
    public void SetIconImages()
    {
        menssage.text = "";
        teamNumber = horizontalSelector.index;
        for (int i = 0; i < 7; i++)
        {
            if (AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[i].name == "")
            {
                icons[i].sprite = Resources.Load<Sprite>("CreaturesIcons/NoneIcon");
            }
            else
            {
                icons[i].sprite = Resources.Load<Sprite>("CreaturesIcons/" + AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[i].name + "Icon");
            }
        }
    }
    public void SetPreVisualizeIcons()
    {
        StartCoroutine("SetPrev");
    }
    IEnumerator SetPrev()
    {
        yield return new WaitUntil(() => NetworkSettings.instance.bcConnected);
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 7; i++)
        {
            if (AcessCreatures.instance.mainTeam.playerTeam[i].name == "")
            {
                prevIcons[i].sprite = Resources.Load<Sprite>("CreaturesIcons/NoneIcon");
            }
            else
            {
                prevIcons[i].sprite = Resources.Load<Sprite>("CreaturesIcons/" + AcessCreatures.instance.mainTeam.playerTeam[i].name + "Icon");
            }
        }
    }
    public void SetMainTeam() {
        List<string> creatureNames = new List<string>();
        int teamAmount = 0;
        bool sameCreature = false;
        teamNumber = horizontalSelector.index;
        AcessCreatures.instance.mainTeam = AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber];
        foreach (var item in AcessCreatures.instance.mainTeam.playerTeam)
        {
            if (item.name != "")
            {
                teamAmount++;
                if (creatureNames.Count > 0)
                {
                    foreach (var name in creatureNames)
                    {
                        if (name == item.name)
                        {
                            sameCreature = true;
                        }
                    }
                }
                creatureNames.Add(item.name);
            }
        }
        if (teamAmount < 7)
        {
            menssage.text = "Not a valid team. Choose all seven creatures";
            AcessCreatures.instance.mainTeam = new AcessCreatures.PlayerTeam();
        }
        else if (sameCreature)
        {
            menssage.text = "Not a valid team. You need choose seven different creature";
            AcessCreatures.instance.mainTeam = new AcessCreatures.PlayerTeam();
        }
        else
        {
            string json = JsonUtility.ToJson(AcessCreatures.instance.mainTeam);
            AcessCreatures.instance.SaveMainTeam(json);
            menssage.text = "";
            SetPreVisualizeIcons();
            loadOut.Play("Panel Out");
        }
    }
}
