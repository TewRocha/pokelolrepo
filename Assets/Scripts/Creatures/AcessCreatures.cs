using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using PlayFab;
using PlayFab.ClientModels;
public class AcessCreatures : MonoBehaviour
{
    [Serializable]
    public class Creature
    {
        public int id;
        public string name;
        public string model;
        public string icon;
        public string[] types;
        public string[] passives;
        public int HP;
        public int STM;
        public int ATK;
        public int DEF;
        public int SATK;
        public int SDEF;
        public int SPD;
    }
    [Serializable]
    public class Creatures
    {
        public Creature[] creatures;
    }
    [Serializable]
    public class Move
    {
        public int id;
        public string name;
        public int power;
        public string type;
        public string classe;
        public int priotiry;
        public int manaCost;
        public int makeCOntact;
        public string[] creaturesCanLearn;
    }
    [Serializable]
    public class Moves
    {
        public Move[] moves;
    }
    [Serializable]
    public class Itens
    {
        public string[] itens;
    }

    [Serializable]
    public class PlayerCreature
    {
        public string name;
        public string passiva;
        public string item;
        public List<string> moves;
        public List<int> evs;
    }
    [Serializable]
    public class PlayerTeam
    {
        public PlayerCreature[] playerTeam;
    }
    [Serializable]
    public class PlayerAllTeams
    {
        public PlayerTeam[] playerTeams;
    }
    public PlayerAllTeams playerTeamOnSave;
    public static AcessCreatures instance;
    public Creatures creatures;
    public Moves moves;
    public Itens itens;
    public TextAsset json;
    public PlayerTeam mainTeam;
    public PlayerTeam battleTeam;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void SetCreatures()
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
            result =>
            {
                if (result.Data == null || !result.Data.ContainsKey("BaseCreatures")) Debug.Log("No BaseCreatures");
                else creatures = JsonUtility.FromJson<Creatures>(result.Data["BaseCreatures"]);
            },
            error =>
            {
                Debug.Log("Got error getting titleData:");
                Debug.Log(error.GenerateErrorReport());
            }
        );
        // creatures = JsonUtility.FromJson<Creatures>(json.text);
    }
    public void SetMoves()
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
            result =>
            {
                if (result.Data == null || !result.Data.ContainsKey("BaseMoves")) Debug.Log("No BaseMoves");
                else moves = JsonUtility.FromJson<Moves>(result.Data["BaseMoves"]);
            },
            error =>
            {
                Debug.Log("Got error getting titleData:");
                Debug.Log(error.GenerateErrorReport());
            }
        );
        //moves = JsonUtility.FromJson<Moves>(json.text);
    }
    public void SetItens()
    {
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
            result =>
            {
                if (result.Data == null || !result.Data.ContainsKey("BaseItens")) Debug.Log("No BaseMoves");
                else itens = JsonUtility.FromJson<Itens>(result.Data["BaseItens"]);
            },
            error =>
            {
                Debug.Log("Got error getting titleData:");
                Debug.Log(error.GenerateErrorReport());
            }
        );
        //itens = JsonUtility.FromJson<Itens>(json.text);
    }
    public void SaveTeams(string json){
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest() {
            Data = new Dictionary<string, string>() {
                {"allTeams", json}
            }
        },
        result => Debug.Log("Successfully updated user data"),
        error => {
            Debug.Log("Got error setting user data Ancestor to Arthur");
            Debug.Log(error.GenerateErrorReport());
        });
    }
    public void SaveMainTeam(string json)
    {
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest()
        {
            Data = new Dictionary<string, string>() {
                {"mainTeam", json}
            }
        },
        result => Debug.Log("Successfully updated user data"),
        error => {
            Debug.Log("Got error setting user data Ancestor to Arthur");
            Debug.Log(error.GenerateErrorReport());
        });
    }
    public void LoadTeams(string myPlayFabeId){
        PlayFabClientAPI.GetUserData(new GetUserDataRequest() {
        PlayFabId = myPlayFabeId,
        Keys = null
        }, result => {
            if (result.Data == null || !result.Data.ContainsKey("allTeams")) Debug.Log("No teams");
            else playerTeamOnSave = JsonUtility.FromJson<PlayerAllTeams>(result.Data["allTeams"].Value);
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport());
        });
    }
    public void LoadMainTeam(string myPlayFabeId)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest()
        {
            PlayFabId = myPlayFabeId,
            Keys = null
        }, result => {
            if (result.Data == null || !result.Data.ContainsKey("allTeams")) Debug.Log("No teams");
            else mainTeam = JsonUtility.FromJson<PlayerTeam>(result.Data["mainTeam"].Value);
        }, (error) => {
            Debug.Log("Got error retrieving user data:");
            Debug.Log(error.GenerateErrorReport());
        });
    }
}
