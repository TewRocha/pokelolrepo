using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CreatureButtonVisual : MonoBehaviour
{
    public static bool set;
    public int creatureNumber;
    public Image icon1, icon2;
    public TMP_Text text1, text2;
    private int teamNumber;
    void OnEnable()
    {
        StartCoroutine(Set(teamNumber));
    }
    public void SetTeam(int value)
    {
        StartCoroutine(Set(value));
    }
    IEnumerator Set(int value)
    {
        yield return new WaitUntil(() => AcessCreatures.instance.creatures.creatures.Length >= 12);
        teamNumber = value;
        if (AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber].name == "")
        {
            icon1.sprite = Resources.Load<Sprite>("CreaturesIcons/NoneIcon");
            icon2.sprite = Resources.Load<Sprite>("CreaturesIcons/NoneIcon");
            text1.text = "None";
            text2.text = "None";
        }
        else
        {
            icon1.sprite = Resources.Load<Sprite>("CreaturesIcons/" + AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber].name + "Icon");
            icon2.sprite = Resources.Load<Sprite>("CreaturesIcons/" + AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber].name + "Icon");
            text1.text = AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber].name;
            text2.text = AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber].name;
        }
    }
}
