using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;
using TMPro;
public class TeamMakerManager : MonoBehaviour
{
    private int teamNumber, creatureNumber;
    [SerializeField]
    private Image iconCreature;
    [SerializeField]
    private CustomDropdown creatureDropdown;
    [SerializeField]
    private CustomDropdown passiveDropdown;
    [SerializeField]
    private CustomDropdown itemDropdown;
    [SerializeField]
    private CustomDropdown move1Dropdown;
    [SerializeField]
    private CustomDropdown move2Dropdown;
    [SerializeField]
    private CustomDropdown move3Dropdown;
    [SerializeField]
    private CustomDropdown move4Dropdown;
    [SerializeField]
    private TMP_InputField creatureInput;
    [SerializeField]
    private TMP_InputField passiveInput;
    [SerializeField]
    private TMP_InputField itemInput;
    [SerializeField]
    private TMP_InputField move1Input;
    [SerializeField]
    private TMP_InputField move2Input;
    [SerializeField]
    private TMP_InputField move3Input;
    [SerializeField]
    private TMP_InputField move4Input;
    [SerializeField]
    private TMP_Text type1;
    [SerializeField]
    private TMP_Text type2;
    [SerializeField]
    private TMP_Text hpText;
    [SerializeField]
    private TMP_Text stmText;
    [SerializeField]
    private TMP_Text atkText;
    [SerializeField]
    private TMP_Text defText;
    [SerializeField]
    private TMP_Text satkText;
    [SerializeField]
    private TMP_Text sdefText;
    [SerializeField]
    private TMP_Text spdText;
    [SerializeField]
    private TMP_Text remainingEvText;
    [SerializeField]
    private Slider hpSlider;
    [SerializeField]
    private Slider stmSlider;
    [SerializeField]
    private Slider atkSlider;
    [SerializeField]
    private Slider defSlider;
    [SerializeField]
    private Slider satkSlider;
    [SerializeField]
    private Slider sdefSlider;
    [SerializeField]
    private Slider spdSlider;
    [SerializeField]
    private Transform creature3dPlace;
    [SerializeField]
    private Image splashImage;
    [SerializeField]
    private ModalWindowManager popup3dObject;
    [SerializeField]
    private NotificationManager notification;
    private AcessCreatures.PlayerTeam playerTeam;
    private AcessCreatures.PlayerCreature playerCreature = new AcessCreatures.PlayerCreature();
    private AcessCreatures.Creature actualCreature;
    private float remainingEv;
    private GameObject modelPrefab,realModel;
    [SerializeField]
    private GameObject creatureLateralButtons;
    [SerializeField]
    private TMP_Text title;
    void Start()
    {
        actualCreature = new AcessCreatures.Creature();
        StartCoroutine("SetCreaturesOnDropdown");
        Invoke("FirstLoad",1);
    }
    void FirstLoad()
    {
        playerCreature = AcessCreatures.instance.playerTeamOnSave.playerTeams[0].playerTeam[0];
        LoadCreature();
    }
    IEnumerator SetCreaturesOnDropdown()
    {
        yield return new WaitUntil(()=> AcessCreatures.instance.creatures.creatures.Length >= 12);
        foreach (AcessCreatures.Creature creature in AcessCreatures.instance.creatures.creatures)
        {
            if (creature.name != "")
            {
                string tempString = "     " + creature.name;
                Sprite sprite = Resources.Load<Sprite>("CreaturesIcons/"+creature.name+"Icon");
                creatureDropdown.CreateNewItem(tempString, sprite);
            }
        }
        SetItensOnDropdown();
    }
    public void LoadCreatureFromButtonsTeam(int value)
    {
        teamNumber = value;
        playerCreature = AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber];
        LoadCreature();
    }
    public void LoadCreatureFromButtonsCreature(int value)
    {
        creatureNumber = value;
        playerCreature = AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber];
        LoadCreature();
    }
    public void SetActualCreature()
    {
        foreach (AcessCreatures.Creature creature in AcessCreatures.instance.creatures.creatures)
        {
            if (creature.name != "")
            {
                if(("     "+creature.name) == creatureInput.text){
                    actualCreature = creature;
                    SetupSplashImage();
                    SetPassivesOnDropdown();
                    SetMovesOnDropDown();
                    SetTypesOnUI();
                    SetBasesOnUI();
                    CheckRemainingEV();
                    title.text = playerCreature.name;
                }
            }
        }
    }
    void SetCreatureByPlayerCreature()
    {
        if (playerCreature.name == "")
        {
            ResetUI();
            return;
        }
        foreach (AcessCreatures.Creature creature in AcessCreatures.instance.creatures.creatures)
        {
            if (creature.name == playerCreature.name)
            {
                actualCreature = creature;
                SetupSplashImage();
                SetPassivesOnDropdown();
                SetMovesOnDropDown();
                SetTypesOnUI();
                SetBasesOnUI();
                CheckRemainingEV();
                creatureInput.text = actualCreature.name;
                iconCreature.sprite = Resources.Load<Sprite>("CreaturesIcons/"+ actualCreature.name + "Icon");
                title.text = playerCreature.name;
            }
        }
    }

    public void SetPassivesOnDropdown(){
        passiveDropdown.gameObject.SetActive(false);
        passiveDropdown.dropdownItemsFilter.Clear();
        passiveDropdown.dropdownItems.Clear();
        passiveDropdown.SetupDropdown();
        passiveDropdown.gameObject.SetActive(true);

        foreach (var passive in actualCreature.passives)
        {
            passiveDropdown.CreateNewItem(passive, Resources.Load<Sprite>("CreaturesIcons/transparentQuad"));
        }
    }

    public void SetMovesOnDropDown(){
        move1Dropdown.gameObject.SetActive(false);
        move1Dropdown.dropdownItemsFilter.Clear();
        move1Dropdown.dropdownItems.Clear();
        move1Dropdown.SetupDropdown();
        move1Dropdown.gameObject.SetActive(true);

        move2Dropdown.gameObject.SetActive(false);
        move2Dropdown.dropdownItemsFilter.Clear();
        move2Dropdown.dropdownItems.Clear();
        move2Dropdown.SetupDropdown();
        move2Dropdown.gameObject.SetActive(true);

        move3Dropdown.gameObject.SetActive(false);
        move3Dropdown.dropdownItemsFilter.Clear();
        move3Dropdown.dropdownItems.Clear();
        move3Dropdown.SetupDropdown();
        move3Dropdown.gameObject.SetActive(true);
        
        move4Dropdown.gameObject.SetActive(false);
        move4Dropdown.dropdownItemsFilter.Clear();
        move4Dropdown.dropdownItems.Clear();
        move4Dropdown.SetupDropdown();
        move4Dropdown.gameObject.SetActive(true);

        foreach (var move in AcessCreatures.instance.moves.moves)
        {
            foreach (var creature in move.creaturesCanLearn)
            {
                if(creature == actualCreature.name){
                    move1Dropdown.CreateNewItem(move.name, Resources.Load<Sprite>("CreaturesIcons/transparentQuad"));
                    move2Dropdown.CreateNewItem(move.name, Resources.Load<Sprite>("CreaturesIcons/transparentQuad"));
                    move3Dropdown.CreateNewItem(move.name, Resources.Load<Sprite>("CreaturesIcons/transparentQuad"));
                    move4Dropdown.CreateNewItem(move.name, Resources.Load<Sprite>("CreaturesIcons/transparentQuad"));
                }
            }
        }
    }
    public void SetTypesOnUI(){
        
        if(actualCreature.types.Length == 1){
            type1.text = actualCreature.types[0];
            type2.text = "";
        }else if(actualCreature.types.Length == 2){
            type1.text = actualCreature.types[0];
            type2.text = actualCreature.types[1];
        }else{
            type1.text = "";
            type2.text = "";
        }
    }
    public void SetBasesOnUI(){
        float temp = 0;
        temp = actualCreature.HP + hpSlider.value;
        hpText.text = temp.ToString();
        temp = actualCreature.STM + stmSlider.value;
        stmText.text = temp.ToString();
        temp = actualCreature.ATK + atkSlider.value;
        atkText.text = temp.ToString();
        temp = actualCreature.DEF + defSlider.value;
        defText.text = temp.ToString();
        temp = actualCreature.SATK + satkSlider.value;
        satkText.text = temp.ToString();
        temp = actualCreature.SDEF + sdefSlider.value;
        sdefText.text = temp.ToString();
        temp = actualCreature.SPD + spdSlider.value;
        spdText.text = temp.ToString();
    }

    public void SetStatsValueOnSlider(string sliderType){
        float temp = 0;
        switch (sliderType)
        {
            case "hp":
                temp = actualCreature.HP + hpSlider.value;
                hpText.text = temp.ToString();
                break;
            case "stm":
                temp = actualCreature.STM + stmSlider.value;
                stmText.text = temp.ToString();
                break;
            case "atk":
                temp = actualCreature.ATK + atkSlider.value;
                atkText.text = temp.ToString();
                break;
            case "def":
                temp = actualCreature.DEF + defSlider.value;
                defText.text = temp.ToString();
                break;
            case "satk":
                temp = actualCreature.SATK + satkSlider.value;
                satkText.text = temp.ToString();
                break;
            case "sdef":
                temp = actualCreature.SDEF + sdefSlider.value;
                sdefText.text = temp.ToString();
                break;
            case "spd":
                temp = actualCreature.SPD + spdSlider.value;
                spdText.text = temp.ToString();
                break;
        }
    }
    public void CheckRemainingEV(){
        remainingEv = 100;
        remainingEv -= hpSlider.value;
        remainingEv -= stmSlider.value;
        remainingEv -= atkSlider.value;
        remainingEv -= defSlider.value;
        remainingEv -= satkSlider.value;
        remainingEv -= sdefSlider.value;
        remainingEv -= spdSlider.value;
        remainingEvText.text = remainingEv.ToString();
    }
    public void StopSliderOnRemaining(string sliderType){
        switch (sliderType)
        {
            case("hp"):
                if(remainingEv<0){
                    hpSlider.value = hpSlider.value-1;
                }
            break;
            case("stm"):
                if(remainingEv<0){
                    stmSlider.value = stmSlider.value-1;
                }
            break;
            case("atk"):
                if(remainingEv<0){
                    atkSlider.value = atkSlider.value-1;
                }
            break;
            case("def"):
                if(remainingEv<0){
                    defSlider.value = defSlider.value-1;
                }
            break;
            case("satk"):
                if(remainingEv<0){
                    satkSlider.value = satkSlider.value-1;
                }
            break;
            case("sdef"):
                if(remainingEv<0){
                    sdefSlider.value = sdefSlider.value-1;
                }
            break;
            case("spd"):
                if(remainingEv<0){
                    spdSlider.value = spdSlider.value-1;
                }
            break;
        }
    }
    public void Set3DModel(){
        popup3dObject.titleText = actualCreature.name;
        foreach(Transform item in creature3dPlace)
        {
            Destroy(item.gameObject);
        }
        modelPrefab = Resources.Load<GameObject>("CreatureModels/" + actualCreature.name + "Prefab");
        realModel = Instantiate(modelPrefab,creature3dPlace.position,creature3dPlace.rotation);
        realModel.transform.parent = creature3dPlace;
        popup3dObject.OpenWindow();
    }
    public void SetupSplashImage(){
        splashImage.sprite = Resources.Load<Sprite>("CreaturesSplashes/" + actualCreature.name + "Splash");
    }
    public void SetItensOnDropdown(){
        foreach (var item in AcessCreatures.instance.itens.itens)
        {
            itemDropdown.CreateNewItem(item, Resources.Load<Sprite>("CreaturesIcons/transparentQuad"));
        }
    }
    public void SaveCreature(){
        playerCreature.name = actualCreature.name;
        playerCreature.passiva = passiveInput.text;
        playerCreature.item = itemInput.text;
        playerCreature.moves[0] = move1Input.text;
        playerCreature.moves[1] = move2Input.text;
        playerCreature.moves[2] = move3Input.text;
        playerCreature.moves[3] = move4Input.text;
        playerCreature.evs[0] = (int)hpSlider.value;
        playerCreature.evs[1] = (int)stmSlider.value;
        playerCreature.evs[2] = (int)atkSlider.value;
        playerCreature.evs[3] = (int)defSlider.value;
        playerCreature.evs[4] = (int)satkSlider.value;
        playerCreature.evs[5] = (int)sdefSlider.value;
        playerCreature.evs[6] = (int)spdSlider.value;

        if(playerCreature.name == ""){
            notification.title = "Can't save yet!";
            notification.description = "You need select a creature";
            notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().bad;
            notification.OpenNotification();
        }else{
            if(playerCreature.passiva == ""){
                notification.title = "Can't save yet!";
                    notification.description = "You need select a passive";
                notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().bad;
                notification.OpenNotification();
            }else{
                if(playerCreature.item == ""){
                    notification.title = "Can't save yet!";
                    notification.description = "You need select a item";
                    notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().bad;
                    notification.OpenNotification();
                }else{
                    if(move1Input.text == "" || move2Input.text == "" || move3Input.text == "" || move4Input.text == ""){
                        notification.title = "Can't save yet!";
                        notification.description = "You need choose all moves";
                        notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().bad;
                        notification.OpenNotification();
                    }else{
                        if(move1Input.text == move2Input.text || move1Input.text == move3Input.text || move1Input.text == move4Input.text
                        || move2Input.text == move3Input.text || move2Input.text == move4Input.text || move3Input.text == move4Input.text){
                            notification.title = "Can't save yet!";
                            notification.description = "You need choose 4 diferent moves";
                            notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().bad;
                            notification.OpenNotification();
                        }else{
                            if(remainingEv > 0){
                                notification.title = "Can't save yet!";
                                notification.description = "You need use all your remaining points";
                                notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().bad;
                                notification.OpenNotification();
                            }else{
                                notification.title = "Great!";
                                notification.description = "Creature saved";
                                notification.gameObject.GetComponent<UIManagerNotification>().background.color = notification.gameObject.GetComponent<UIManagerNotification>().good;
                                notification.OpenNotification();
                                AcessCreatures.instance.playerTeamOnSave.playerTeams[teamNumber].playerTeam[creatureNumber] = playerCreature;
                                string json = JsonUtility.ToJson(AcessCreatures.instance.playerTeamOnSave);
                                AcessCreatures.instance.SaveTeams(json);
                                creatureLateralButtons.SetActive(false);
                                creatureLateralButtons.SetActive(true);
                                title.text = playerCreature.name;
                            }
                        }
                    }
                }
            }
        }
    }
    public void LoadCreature(){
        ResetUI();
        var temp = playerCreature;
        playerCreature.name = temp.name;
        SetCreatureByPlayerCreature();
        playerCreature.passiva = temp.passiva;
        passiveInput.text = temp.passiva;
        playerCreature.item = temp.item;
        itemInput.text = temp.item;
        playerCreature.moves[0] = temp.moves[0];
        move1Input.text = temp.moves[0];
        playerCreature.moves[1] = temp.moves[1];
        move2Input.text = temp.moves[1];
        playerCreature.moves[2] = temp.moves[2];
        move3Input.text = temp.moves[2];
        playerCreature.moves[3] = temp.moves[3];
        move4Input.text = temp.moves[3];
        playerCreature.evs[0] = temp.evs[0];
        hpSlider.value = temp.evs[0];
        playerCreature.evs[1] = temp.evs[1];
        stmSlider.value = temp.evs[1];
        playerCreature.evs[2] = temp.evs[2];
        atkSlider.value = temp.evs[2];
        playerCreature.evs[3] = temp.evs[3];
        defSlider.value = temp.evs[3];
        playerCreature.evs[4] = temp.evs[4];
        satkSlider.value = temp.evs[4];
        playerCreature.evs[5] = temp.evs[5];
        sdefSlider.value = temp.evs[5];
        playerCreature.evs[6] = temp.evs[6];
        spdSlider.value = temp.evs[6];
        CheckRemainingEV();
        NetworkSettings.instance.allCreaturesLoaded++;
    }
    public void ResetUI()
    {
        //Reset passive
        passiveDropdown.gameObject.SetActive(false);
        passiveDropdown.dropdownItemsFilter.Clear();
        passiveDropdown.dropdownItems.Clear();
        passiveDropdown.SetupDropdown();
        passiveDropdown.gameObject.SetActive(true);


        //ResetMoves
        move1Dropdown.gameObject.SetActive(false);
        move1Dropdown.dropdownItemsFilter.Clear();
        move1Dropdown.dropdownItems.Clear();
        move1Dropdown.SetupDropdown();
        move1Dropdown.gameObject.SetActive(true);

        move2Dropdown.gameObject.SetActive(false);
        move2Dropdown.dropdownItemsFilter.Clear();
        move2Dropdown.dropdownItems.Clear();
        move2Dropdown.SetupDropdown();
        move2Dropdown.gameObject.SetActive(true);

        move3Dropdown.gameObject.SetActive(false);
        move3Dropdown.dropdownItemsFilter.Clear();
        move3Dropdown.dropdownItems.Clear();
        move3Dropdown.SetupDropdown();
        move3Dropdown.gameObject.SetActive(true);

        move4Dropdown.gameObject.SetActive(false);
        move4Dropdown.dropdownItemsFilter.Clear();
        move4Dropdown.dropdownItems.Clear();
        move4Dropdown.SetupDropdown();
        move4Dropdown.gameObject.SetActive(true);


        //Reset Types
        type1.text = "None";
        type2.text = "";


        //Reset Sliders values
        hpSlider.value = 0;
        stmSlider.value = 0;
        atkSlider.value = 0;
        defSlider.value = 0;
        satkSlider.value = 0;
        sdefSlider.value = 0;
        spdSlider.value = 0;


        //Sliders UI Values
        hpText.text = "0";
        stmText.text = "0";
        atkText.text = "0";
        defText.text = "0";
        satkText.text = "0";
        sdefText.text = "0";
        spdText.text = "0";

        //Reset SplashImage
        splashImage.sprite = Resources.Load<Sprite>("CreaturesSplashes/NoneSplash");


        //ResetCreatureDropdown
        creatureInput.text = "Select a creature";
        iconCreature.sprite = Resources.Load<Sprite>("CreaturesIcons/NoneIcon");

        //ResetTitle
        title.text = "None";
    }
}
